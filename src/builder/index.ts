/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_PHONE from "../../assets/phone.svg";
import ICON_TABLET from "../../assets/tablet.svg";
import ICON_DESKTOP from "../../assets/desktop.svg";

@tripetto({
    type: "condition",
    context: "*",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    autoOpen: true,
    get label() {
        return pgettext("block:device", "Device");
    },
})
export class DeviceCondition extends ConditionBlock {
    @definition
    @affects("#name")
    deviceType: "phone" | "tablet" | "desktop" = "desktop";

    get name() {
        switch (this.deviceType) {
            case "phone":
                return pgettext("block:device", "Phone");
            case "tablet":
                return pgettext("block:device", "Tablet");
            case "desktop":
                return pgettext("block:device", "Desktop");
        }
    }

    get icon() {
        switch (this.deviceType) {
            case "phone":
                return ICON_PHONE;
            case "tablet":
                return ICON_TABLET;
            case "desktop":
                return ICON_DESKTOP;
        }
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:device", "Branch on device type"),
            controls: [
                new Forms.Radiobutton(
                    [
                        {
                            value: "desktop",
                            label: pgettext("block:device", "Desktop"),
                            description: pgettext(
                                "block:device",
                                "Large screen device (probably a laptop or desktop)."
                            ),
                        },
                        {
                            value: "tablet",
                            label: pgettext("block:device", "Tablet"),
                            description: pgettext(
                                "block:device",
                                "Medium screen device."
                            ),
                        },
                        {
                            value: "phone",
                            label: pgettext("block:device", "Phone"),
                            description: pgettext(
                                "block:device",
                                "Small screen device."
                            ),
                        },
                    ],
                    Forms.Dropdown.bind(this, "deviceType", "desktop")
                ),
            ],
        });
    }
}
