/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Num,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class DeviceCondition extends ConditionBlock<{
    readonly deviceType: "phone" | "tablet" | "desktop";
}> {
    @condition
    evaluateDevice(): boolean {
        if (window) {
            const size =
                (this.view === "live" &&
                    window.screen &&
                    Num.min(window.screen.width, window.screen.height)) ||
                Num.min(window.innerWidth, window.innerHeight);

            if (size < 400) {
                return this.props.deviceType === "phone";
            } else if (size < 800) {
                return this.props.deviceType === "tablet";
            }

            return this.props.deviceType === "desktop";
        }

        return false;
    }
}
